package org.apache.bigtop.manager.common.message.entity.payload;

import lombok.Data;

@Data
public class BasePayload {

    private String hostname;
}
