package org.apache.bigtop.manager.common.message.entity.payload;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.bigtop.manager.common.message.entity.pojo.HostCheckType;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class HostCheckPayload extends BasePayload {

    private HostCheckType[] hostCheckTypes;
}
