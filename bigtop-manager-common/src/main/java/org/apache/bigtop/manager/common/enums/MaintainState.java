package org.apache.bigtop.manager.common.enums;

import lombok.Getter;

@Getter
public enum MaintainState {
    UNINSTALLED,

    INSTALLED,

    MAINTAINED,

    STARTED,

    STOPPED,

}
