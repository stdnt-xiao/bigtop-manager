package org.apache.bigtop.manager.common.enums;

public enum JobState {

    PENDING,

    PROCESSING,

    SUCCESSFUL,

    FAILED,

    CANCELED

}
