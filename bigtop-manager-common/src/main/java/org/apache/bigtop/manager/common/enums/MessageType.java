package org.apache.bigtop.manager.common.enums;

public enum MessageType {

    COMMAND,

    HOST_CHECK,

    CACHE_DISTRIBUTE,
}
