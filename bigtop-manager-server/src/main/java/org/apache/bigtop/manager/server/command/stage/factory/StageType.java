package org.apache.bigtop.manager.server.command.stage.factory;

public enum StageType {

    HOST_CHECK,

    CACHE_DISTRIBUTE,

    COMPONENT_INSTALL,

    COMPONENT_START,

    COMPONENT_STOP,

    COMPONENT_CHECK,

    SERVICE_CHECK,

    SERVICE_CONFIGURE,
    ;
}
